/* Kernel includes. */
#include "funfa/include/FreeRTOS.h"
#include "funfa/include/task.h"
#include "funfa/include/semphr.h"

SemaphoreHandle_t traffic;

uint8_t *v, i;

static void controller( void *pvParameters ) {
    while(1) {
		xSemaphoreGive(traffic);
        vTaskDelay(500);
    }
}

static void RLed( void *pvParameters) {
	while(1) {
		xSemaphoreTake(traffic, portMAX_DELAY);
		P1OUT ^= BIT0;
	}
}

static void GLed( void *pvParameters) {
	while(1) {
		xSemaphoreTake(traffic, portMAX_DELAY);
		P4OUT ^= BIT7;
	}
}

int main( void )
{
    /* Configure system. */
    WDTCTL = WDTPW | WDTHOLD;
//  PM5CTL0 &= ~LOCKLPM5;           //Para destravar os pinos na FR2355
    P1DIR |= BIT0;
    P4DIR |= BIT7;

    traffic = xSemaphoreCreateCounting(1,1);
    if(traffic == NULL) return 1;
    /* Create the Task */
    xTaskCreate( controller,                             /* The function that implements the task. */
                "Controller",                            /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                configMINIMAL_STACK_SIZE,           /* The size of the stack to allocate to the task. */
                NULL,                               /* The parameter passed to the task - not used in this case. */
                2,                                  /* The priority assigned to the task. */
                NULL );                             /* The task handle is not required, so NULL is passed. */

    xTaskCreate( RLed,                             /* The function that implements the task. */
                 "RLed",                            /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                 configMINIMAL_STACK_SIZE,           /* The size of the stack to allocate to the task. */
                 NULL,                               /* The parameter passed to the task - not used in this case. */
                 2,                                  /* The priority assigned to the task. */
                 NULL );

    xTaskCreate( GLed,                             /* The function that implements the task. */
                 "GLed",                            /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                 configMINIMAL_STACK_SIZE,           /* The size of the stack to allocate to the task. */
                 NULL,                               /* The parameter passed to the task - not used in this case. */
                 2,                                  /* The priority assigned to the task. */
                 NULL );

    /* Start RTOS */
    vTaskStartScheduler();
    return 0;
}
